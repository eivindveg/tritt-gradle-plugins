package no.tritt;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

import java.util.Map;

@Controller("/hello")
public class HelloController {

    @Get
    public Map<String, String> hello() {
        return Map.of("hello", "tritt");
    }

}
