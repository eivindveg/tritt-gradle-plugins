package tritt.example;

import no.tritt.problem.Problem;

public class UseProblem {

    private final Problem problem;

    public UseProblem(Problem problem) {
        this.problem = problem;
    }

    public Problem getProblem() {
        return problem;
    }
}
