package no.tritt.gradle

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.testing.Test
import org.gradle.api.tasks.testing.TestDescriptor
import org.gradle.api.tasks.testing.TestResult
import org.gradle.api.tasks.testing.logging.TestLogEvent
import org.gradle.api.tasks.testing.logging.TestLoggingContainer

@CompileStatic
class TestOutputConventionsPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {

        project.getTasks().withType(Test.class).configureEach { Test t ->
            t.useJUnitPlatform()

            boolean showTestOutput = project.hasProperty("show-test-output")
            if (showTestOutput) {
                t.testLogging { TestLoggingContainer l ->
                    l.setShowStandardStreams(true)
                }
            }

            boolean showTests = project.hasProperty("verbose") || project.hasProperty("show-tests")
            if (showTests) {
                t.testLogging { TestLoggingContainer l ->
                    l.setDisplayGranularity(1)
                    l.events(
                        TestLogEvent.PASSED,
                        TestLogEvent.SKIPPED,
                        TestLogEvent.FAILED
                    )
                }
            }

            boolean showSummary = showTests || project.hasProperty("show-summary")
            if (showSummary) {
                t.afterSuite { TestDescriptor td, TestResult tr ->
                    if (!td.parent) { // will match the outermost suite
                        project.logger.lifecycle(
                            '\n{}: {} ({} tests, PASSED: {}, FAILED: {}, SKIPPED: {})',
                            td.displayName,
                            tr.resultType,
                            tr.testCount,
                            tr.successfulTestCount,
                            tr.failedTestCount,
                            tr.skippedTestCount
                        )
                    }
                }
            }
        }

    }
}
