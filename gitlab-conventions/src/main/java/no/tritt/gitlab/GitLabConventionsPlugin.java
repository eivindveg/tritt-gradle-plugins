package no.tritt.gitlab;

import org.gradle.api.NamedDomainObjectContainer;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.logging.Logger;
import org.gradle.api.model.ObjectFactory;
import org.gradle.api.publish.PublishingExtension;
import org.gradle.api.publish.maven.plugins.MavenPublishPlugin;

import static no.tritt.gitlab.GitLabAware.DEFAULT_NAME_GITLAB;

public class GitLabConventionsPlugin implements Plugin<Project> {

    public static final String EXTENSION_GITLAB = "gitlab";
    public static final String CONTAINER_GITLAB_REPOSITORIES = "gitlabRepositories";

    @Override
    public void apply(Project project) {
        Logger logger = project.getLogger();

        project.getExtensions().create(EXTENSION_GITLAB, GitLabExtension.class);

        GitLabProperties glProps = new GitLabProperties(project);
        if (!glProps.isEmpty()) {
            GitLabRepositoryAction repoFromExtension = new GitLabRepositoryAction(DEFAULT_NAME_GITLAB, glProps);
            logger.info("Adding repository: {}", repoFromExtension.describeRepository());
            project.getRepositories().maven(repoFromExtension);
        }

        glProps.getPublishToProjectId().ifPresent(publishToProjectId ->
            project.getPlugins().withType(MavenPublishPlugin.class).configureEach((MavenPublishPlugin mpp) -> {
                GitLabRepositoryAction repoForProjectId = toRepositoryAction(glProps, publishToProjectId);
                logger.info("Adding publishing repository: {}", repoForProjectId.describeRepository());
                PublishingExtension pe = project.getExtensions().getByType(PublishingExtension.class);
                pe.getRepositories().maven(repoForProjectId);
            })
        );

        ObjectFactory objects = project.getObjects();
        NamedDomainObjectContainer<GitLabRepository> container = objects.domainObjectContainer(
            GitLabRepository.class,
            name -> objects.newInstance(GitLabRepository.class, name)
        );
        project.getExtensions().add(CONTAINER_GITLAB_REPOSITORIES, container);

        project.afterEvaluate(p ->
            container.all(glr -> {
                GitLabRepositoryAction repoFromNamed = toRepositoryAction(glr);
                logger.info("Adding repository: {}", repoFromNamed.describeRepository());
                p.getRepositories().maven(repoFromNamed);
            })
        );
    }

    private static GitLabRepositoryAction toRepositoryAction(GitLabProperties glProps, String publishToProjectId) {
        return new GitLabRepositoryAction(DEFAULT_NAME_GITLAB, GitLabMethods.createProjectRepositoryUrl(publishToProjectId), glProps.getToken().orElse(""));
    }

    private static GitLabRepositoryAction toRepositoryAction(GitLabRepository glr) {
        return new GitLabRepositoryAction(
            glr.name,
            glr
        );
    }

}
