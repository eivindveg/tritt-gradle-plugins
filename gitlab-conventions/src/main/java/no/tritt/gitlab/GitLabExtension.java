package no.tritt.gitlab;

import org.gradle.api.Action;
import org.gradle.api.NamedDomainObjectContainer;
import org.gradle.api.Project;
import org.gradle.api.artifacts.repositories.MavenArtifactRepository;

import javax.inject.Inject;
import java.util.Map;

public abstract class GitLabExtension {

    public abstract NamedDomainObjectContainer<GitLabRepository> getRepositories();

    private final Project project;

    @Inject
    public GitLabExtension(Project project) {
        this.project = project;
    }

    /**
     * <pre>
     * <code>
     * repositories {
     *     maven gitlab.repo(name: 'GitLab', groupId: '123456')
     * }
     * </code>
     * </pre>
     *
     * @param params map containing the necessary repository params
     * @return action that configures a Maven repository
     */
    public Action<MavenArtifactRepository> repo(Map<String, Object> params) {
        return new GitLabRepositoryAction(
            params.getOrDefault("name", GitLabAware.DEFAULT_NAME_GITLAB).toString(),
            GitLabMethods.createRepositoryUrl(
                params.getOrDefault("groupId", "").toString(),
                params.getOrDefault("projectId", "").toString()
            ),
            params.getOrDefault("token", "").toString()
        );
    }

    public Action<MavenArtifactRepository> tritt() {
        return new GitLabRepositoryAction(
            GitLabGroup.Tritt.name(),
            GitLabMethods.createGroupRepositoryUrl(GitLabGroup.Tritt.groupId),
            GitLabMethods.gitLabToken(project)
        );
    }

    public Action<MavenArtifactRepository> trittOpen() {
        return new GitLabRepositoryAction(
            GitLabGroup.TrittOpen.name(),
            GitLabMethods.createGroupRepositoryUrl(GitLabGroup.TrittOpen.groupId),
            ""
        );
    }

}
