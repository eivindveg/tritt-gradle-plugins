package no.tritt.gitlab;

import java.util.Optional;

public interface GitLabAware {

    String DEFAULT_NAME_GITLAB = "GitLab";

    String PROPERTY_GITLAB_GROUP_ID = "gitlabGroupId";

    String PROPERTY_GITLAB_PROJECT_ID = "gitlabProjectId";

    String PROPERTY_GITLAB_PUBLISH_TO_PROJECT_ID = "gitlabPublishToProjectId";

    String PROPERTY_GITLAB_PUBLISH_TO_PROJECT = "gitlabPublishToProject";

    String PROPERTY_GITLAB_TOKEN = "gitlabToken";

    String ENV_VAR_GITLAB_TOKEN = "GITLAB_TOKEN";

    Optional<String> getGroupId();

    Optional<String> getProjectId();

    Optional<String> getToken();

    default boolean isEmpty() {
        return getGroupId().isEmpty() && getProjectId().isEmpty();
    }

    default String resolveRepoUrl() {
        return GitLabMethods.createRepositoryUrl(
            getGroupId().orElse(""),
            getProjectId().orElse("")
        );
    }

}
