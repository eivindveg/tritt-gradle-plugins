package no.tritt.gitlab;

import org.gradle.api.Action;
import org.gradle.api.artifacts.repositories.MavenArtifactRepository;
import org.gradle.api.credentials.HttpHeaderCredentials;
import org.gradle.authentication.http.HttpHeaderAuthentication;

public class GitLabRepositoryAction implements Action<MavenArtifactRepository> {

    private final String name;
    private final String url;
    private final String token;

    public GitLabRepositoryAction(String name, GitLabAware gl) {
        this(
            capitalize(name),
            gl.resolveRepoUrl(),
            gl.getToken().orElse("")
        );
    }

    public GitLabRepositoryAction(String name, String url, String token) {
        this.name = name;
        this.url = url;
        this.token = token;
    }

    @Override
    public void execute(MavenArtifactRepository r) {
        r.setName(name);
        r.setUrl(url);
        if (!token.isBlank()) {
            r.authentication(a ->
                a.create("header", HttpHeaderAuthentication.class)
            );
            r.credentials(HttpHeaderCredentials.class, h -> {
                h.setName("Private-Token");
                h.setValue(token);
            });
        }
    }

    public String describeRepository() {
        return "Repository{" +
            "name='" + name + '\'' +
            ", url='" + url + '\'' +
            ", token='" + (token == null || token.isBlank() ? "" : "****") + '\'' +
            '}';
    }

    private static String capitalize(CharSequence s) {
        if (s.length() == 0) {
            return "";
        }
        return "" + Character.toUpperCase(s.charAt(0)) + s.subSequence(1, s.length());
    }

}
