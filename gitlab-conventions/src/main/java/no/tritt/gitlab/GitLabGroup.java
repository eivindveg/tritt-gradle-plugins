package no.tritt.gitlab;

public enum GitLabGroup {
    Tritt("6306164"),
    TrittOpen("12992043");

    public final String groupId;

    GitLabGroup(String groupId) {
        this.groupId = groupId;
    }
}
