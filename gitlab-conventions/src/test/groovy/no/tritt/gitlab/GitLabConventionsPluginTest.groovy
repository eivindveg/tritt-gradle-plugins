package no.tritt.gitlab

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification
import spock.lang.Unroll

class GitLabConventionsPluginTest extends Specification {

    @Rule TemporaryFolder tf = new TemporaryFolder()

    def 'apply plugin, DONT fail if no groupId or projectId is configured'() {
        when:
            BuildResult result = buildAndRunTask(
                // language=gradle
                '''
                plugins {
                    id 'no.tritt.gitlab-conventions'
                }
                ''',
                'tasks'
            )
        then:
            result
    }

    def 'apply plugin, add repositories using convenience methods'() {
        when:
            BuildResult result = buildAndRunTask(
                // language=gradle
                '''
                plugins {
                    id 'no.tritt.gitlab-conventions'
                }
                
                repositories {
                    maven gitlab.tritt()
                    maven gitlab.trittOpen()
                }
                ''',
                'tasks'
            )
        then:
            result
    }

    def 'apply plugin, read #property=#value from gradle.properties'() {
        given:
            tf.newFile('gradle.properties').text = "${property}=${value}"
        when:
            BuildResult result = buildAndRunTask(
                // language=gradle
                '''
                plugins {
                    id 'no.tritt.gitlab-conventions'
                }
                ''',
                'tasks'
            )
        then:
            result
        where:
            property          | value
            'gitlabGroupId'   | '12345'
            'gitlabProjectId' | '12345'
    }

    def 'apply plugin and react to maven-publish and DONT ADD repository when only groupId is present'() {
        given:
            tf.newFile('gradle.properties').text = 'gitlabGroupId=4326879'
        when:
            BuildResult result = buildAndRunTask(
                // language=gradle
                '''
                plugins {
                    id 'no.tritt.gitlab-conventions'
                    id 'maven-publish'
                }
                ''',
                'tasks', '--group', 'publishing'
            )
        then:
            println '--------------------------------'
            println result.output
            println '--------------------------------'
            !result.output.contains('ToGitLabRepository')
    }

    @Unroll
    def 'apply plugin and react to maven-publish by adding repository when #property is present'() {
        given:
            tf.newFile('gradle.properties').text = "${property}=${value}"
        when:
            BuildResult result = buildAndRunTask(
                // language=gradle
                '''
                plugins {
                    id 'no.tritt.gitlab-conventions'
                    id 'maven-publish'
                }
                ''',
                'tasks', '--group', 'publishing'
            )
        then:
            println '--------------------------------'
            println result.output
            println '--------------------------------'
            result.output.contains('ToGitLabRepository')
        where:
            property                   | value
            'gitlabProjectId'          | '4326879'
            'gitlabPublishToProjectId' | '4326879'

    }

    def 'apply plugin with named gitlab containers'() {
        when:
            BuildResult result = buildAndRunTask(
                // language=gradle
                '''
                plugins {
                    id 'no.tritt.gitlab-conventions'
                }
                
                gitlabRepositories {
                    trittOpen {
                        groupId '9098098'
                    }
                    trittProblemJson {
                        projectId '12348978'
                    }
                }
                ''',
                'tasks'
            )
        then:
            println '--------------------------------'
            println result.output
            println '--------------------------------'
    }

    private BuildResult buildAndRunTask(String build, String... arguments) {
        File buildFile = tf.newFile('build.gradle')
        buildFile.text = build
        return GradleRunner.create()
            .withProjectDir(tf.root)
            .withArguments(arguments)
            .withPluginClasspath()
            .build()
    }

}
