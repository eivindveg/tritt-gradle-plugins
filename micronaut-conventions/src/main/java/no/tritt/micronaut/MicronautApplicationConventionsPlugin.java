package no.tritt.micronaut;

import io.micronaut.gradle.MicronautApplicationPlugin;
import io.micronaut.gradle.docker.MicronautDockerfile;
import org.gradle.api.Project;

public class MicronautApplicationConventionsPlugin extends MicronautConventionsPlugin {

    @Override
    public void apply(Project project) {

        project.getPlugins().apply(MicronautApplicationPlugin.class);

        Object dockerBaseImage = project.findProperty("dockerBaseImage");
        if (dockerBaseImage != null) {
            project.getTasks().withType(MicronautDockerfile.class).configureEach(mdf ->
                mdf.baseImage((String) dockerBaseImage)
            );
        }

        applyCommonConventions(project);
    }
}
