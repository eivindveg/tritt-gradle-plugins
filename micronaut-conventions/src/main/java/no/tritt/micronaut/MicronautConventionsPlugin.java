package no.tritt.micronaut;

import io.micronaut.gradle.MicronautExtension;
import io.micronaut.gradle.MicronautTestRuntime;
import org.gradle.api.Plugin;
import org.gradle.api.Project;

import java.util.List;

public abstract class MicronautConventionsPlugin implements Plugin<Project> {

    protected void applyCommonConventions(Project project) {
        MicronautExtension extension = project.getExtensions().getByType(MicronautExtension.class);

        extension.version((String) project.property("micronautVersion"));
        extension.getTestRuntime().convention(MicronautTestRuntime.SPOCK_2);
        extension.processing(p -> {
            p.getIncremental().convention(true);
            p.getAnnotations().convention(List.of("tritt.*"));
        });
    }

}
