package no.tritt.micronaut;

import io.micronaut.gradle.MicronautLibraryPlugin;
import org.gradle.api.Project;

public class MicronautLibraryConventionsPlugin extends MicronautConventionsPlugin {

    @Override
    public void apply(Project project) {

        project.getPlugins().apply(MicronautLibraryPlugin.class);

        applyCommonConventions(project);
    }
}
