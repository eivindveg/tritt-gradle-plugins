package no.tritt.environment;

import org.ajoberstar.reckon.core.Version;
import org.ajoberstar.reckon.gradle.ReckonExtension;
import org.ajoberstar.reckon.gradle.ReckonPlugin;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.internal.file.FileFactory;
import org.gradle.api.tasks.TaskProvider;

import javax.inject.Inject;

public class EnvironmentInformationPlugin implements Plugin<Project> {

    @Inject
    private FileFactory fileFactory;

    @Override
    public void apply(Project project) {
        if (project.getParent() != null) {
            throw new UnsupportedOperationException("This plugin can only be applied to the root project.");
        }
        TaskProvider<WriteEnvironmentTask> writeEnvironmentTask = project.getTasks().register("writeEnvironment", WriteEnvironmentTask.class, writeEnvironment -> {
            writeEnvironment.getOutputFile().convention(
                    project.getLayout()
                            .getBuildDirectory()
                            .file("env/.env")
            );
            writeEnvironment.getVersion().convention(project.getVersion().toString());

            project.getPlugins().withType(ReckonPlugin.class, appliedReckon -> {
                project.getExtensions().configure(ReckonExtension.class, appliedExtension -> {
                    //noinspection NullableProblems
                    writeEnvironment.getVersion().set(project.getProviders().provider(() -> appliedExtension.getVersion()
                            .map(Version::toString)
                            // remove commit metadata
                            .map(string -> string.split("\\+")[0])
                            .get()));
                });
            });
        });

        project.getTasks().register("printEnvironment", PrintEnvironmentTask.class, printEnvironmentTask -> {
            printEnvironmentTask.getInputFile().convention(writeEnvironmentTask.get().getOutputFile());
        });
    }
}
