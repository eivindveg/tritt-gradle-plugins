package no.tritt.reckon;

import org.ajoberstar.reckon.gradle.ReckonExtension;
import org.ajoberstar.reckon.gradle.ReckonPlugin;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.plugins.ExtraPropertiesExtension;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReckonConventionsPlugin implements Plugin<Project> {

    private static final Pattern VERSION_PATTERN = Pattern.compile("(.*)-(\\w+)[.](\\d+)[.](\\d+)[+](.*)");

    private static final String STAGE_BETA = "beta";
    private static final String STAGE_RC = "rc";
    private static final String STAGE_FINAL = "final";

    private static final String PROPERTY_RECKON_SCOPE = "reckon.scope";
    private static final String PROPERTY_RECKON_STAGE = "reckon.stage";
    private static final String PROPERTY_RECKON_TAG = "reckon.tag";

    @Override
    public void apply(Project project) {

        project.getPlugins().apply(ReckonPlugin.class);
        project.getExtensions().getByType(ReckonExtension.class)
            .scopeFromProp()
            .stageFromProp(STAGE_BETA, STAGE_RC, STAGE_FINAL);

        ExtraPropertiesExtension ext = project.getExtensions().getExtraProperties();

        Object reckonTagProperty = project.findProperty(PROPERTY_RECKON_TAG);
        if (reckonTagProperty instanceof String) {
            String reckonTag = (String) reckonTagProperty;
            if (reckonTag.contains(".")) {
                String[] parts = reckonTag.split("[.]", 2);
                ext.set(PROPERTY_RECKON_SCOPE, parts[0]);
                ext.set(PROPERTY_RECKON_STAGE, parts[1]);
            } else {
                ext.set(PROPERTY_RECKON_SCOPE, reckonTag);
                ext.set(PROPERTY_RECKON_STAGE, STAGE_FINAL);
            }
        }

        Object lessChangingVersion = decideLessChangingVersion(project);

        ext.set("lessChangingVersion", lessChangingVersion);
        project.getAllprojects().forEach(p -> {
            p.setVersion(lessChangingVersion);

            p.getTasks().register("showVersion", t ->
                t.doLast(action ->
                    action.getLogger().lifecycle("{}: {}", p.getName(), p.getVersion())
                )
            );
        });
    }

    private Object decideLessChangingVersion(Project project) {
        String versionString = project.getVersion().toString();

        if (project.hasProperty("forcedVersion")) {
            Object forcedVersion = project.property("forcedVersion");
            project.getLogger().lifecycle("Forced version:   {}", forcedVersion);
            return forcedVersion;
        }

        Matcher matcher = VERSION_PATTERN.matcher(versionString);
        if (matcher.matches()) {
            String finalPart = matcher.group(1);

            String snapshotVersion = String.format("%s-SNAPSHOT", finalPart);
            project.getLogger().lifecycle("Snapshot version: {}", snapshotVersion);
            return snapshotVersion;
        }

        return project.getVersion();
    }
}
