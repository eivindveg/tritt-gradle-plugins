package no.tritt.reckon

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class ReckonConventionsPluginTest extends Specification {

    @Rule TemporaryFolder tf = new TemporaryFolder()

    def 'apply plugin'() {
        when:
            BuildResult result = buildAndRunTask(
                // language=gradle
                '''
                plugins {
                    id 'no.tritt.reckon-conventions'
                }
                ''',
                'tasks'
            )
        then:
            println result.output
    }

    private BuildResult buildAndRunTask(String build, String... arguments) {
        File buildFile = tf.newFile('build.gradle')
        buildFile.text = build
        return GradleRunner.create()
            .withProjectDir(tf.root)
            .withArguments(arguments)
            .withPluginClasspath()
            .build()
    }

}
